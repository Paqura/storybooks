const express = require('express');
const path = require('path');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

/**
 * ! Подключение модели всегда раньше пасспорта
 */

require('./models/user');
require('./models/story');

/**
 * * Паспорт конфиг
 */

require('./config/passport')(passport);

/**
 * * Подключение роутов
 */

const index = require('./routes/index');
const auth = require('./routes/auth');
const stories = require('./routes/stories');

/**
 * * Подключение Ключей из конфига
 */

const keys = require('./config/keys');

/**
 * * Handlebars helpers
 */

const { truncate, stripTags, formatDate }  = require('./helpers/hbs');

/**
 * * Подключение Mongoose
 */

mongoose.connect(keys.mongoURI)
  .then(() => console.log('mongo connected'))
  .catch((err) => console.error(err));

/**
 * * Глобальные промимы
 */

mongoose.Promise = global.Promise;

const app = express();
const port = process.env.PORT || 8080;

/**
 * * Подключение парсера
 */

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

/**
 * * Method override middleware
 */

app.use(methodOverride('_method'));

/**
 * * Подключение handlebars middleware
 */

app.engine('handlebars', exphbs({
  helpers: {
    truncate,
    stripTags,
    formatDate
  },
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

/**
 * * Подключение парсера кук и сессии
 */

app.use(cookieParser());
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}));

/**
* * Passport Middleware
*/

app.use(passport.initialize());
app.use(passport.session());

/**
 * * Задание глобальных переменных
 */

app.use((req, res, next) => {
  res.locals.user = req.user || null;
  next();
});

/**
 * * Подключение статики
 */

app.use(express.static(path.join(__dirname, 'public')));

/**
 * * Подключение роутов
 */

app.use('/', index);
app.use('/auth', auth);
app.use('/stories', stories);

app.listen(port, () => {
  console.log(`Сервер запущен на ${port} порту`);
});

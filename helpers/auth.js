module.exports = {
  /**
   * * Если юзер зареган - то всё хорошо,
   * * иначе отсылает на главную
   */
  ensureAuthenticated: function(req, res, next) {
    if(req.isAuthenticated()){
      return next();
    }
    res.redirect('/');
  },
  /**
   * * Если юзер не гость - переходит на истории
   * * с главной страницы,
   * * иначе идет дальше цепочка
   */
  ensureGuest: function(req, res, next) {
    if(req.isAuthenticated()){
      res.redirect('/dashboard');
    } else {
      return next();
    }
  }
};

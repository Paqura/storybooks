const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Story = mongoose.model('stories');
const User = mongoose.model('users');
const { ensureAuthenticated, ensureGuest } = require('../helpers/auth');

router.get('/', ensureAuthenticated, (req, res) => {
  Story.find({status: 'public'})
    .populate('user')
    .then(stories => {
      res.render('stories/index', {
        stories
      });
    })
});

router.get('/add', ensureAuthenticated, (req, res) => {
  res.render('stories/add');
});

router.post('/', (req, res) => {
  let allowComments;

  if(req.body.allowComments) {
    allowComments = true;
  } else {
    allowComments = false;
  }

  const newStory = {
    title: req.body.title,
    status: req.body.status,
    body: req.body.body,
    allowComments,
    user: req.user.id,
    comments: req.body.comments || []
  };

  new Story(newStory)
    .save()
    .then(story => {
      res.redirect(`/stories/show/${story.id}`)
    })
});

router.post('/show/:id', (req, res) => {
  Story.findOne({_id: req.params.id})
    .then(story => {
      const newComment = {
        commentBody: req.body.comment,
        commentUser: req.user.id,
        userName: `${req.user.firstName} ${req.user.lastName}`
      };
      story.comments = story.comments.concat(newComment);
      story.save();
      res.redirect(`/stories/show/${story._id}`);
    });
});

router.get('/show/:id', (req, res) => {
  Story.findOne({_id: req.params.id})
    .populate('user')
    .then(story => {
      let status = false;
      if(story.status === 'public') {
        status = true;
      }
      res.render(`stories/show`, {
        story,
        status: status
      })
    })
});

router.put('/:id', (req, res) => {
  Story.findOne({
    _id: req.params.id
  })
    .then(story => {
      let allowComments;

      if(req.body.allowComments) {
        allowComments = true;
      } else {
        allowComments = false;
      }

      story.title = req.body.title;
      story.body = req.body.body;
      story.status = req.body.status;
      story.allowComments = allowComments;

      story.save()
        .then(story => {
          res.redirect('/dashboard');
      })
    })
});

router.get('/edit/:id', (req, res) => {
  Story.findOne({ _id: req.params.id})
    .then(story => {
      res.render('stories/edit', {
        story
      });
    })
});

router.delete('/:id', (req, res) => {
  Story.deleteOne({_id: req.params.id})
    .then(story => {
      Story.find({})
        .then(stories => {
          res.redirect('/');
      })
    })
});



module.exports = router;
